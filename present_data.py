import numpy as np
import matplotlib.pyplot as plt

fileName = 'C:/Users/anumm/uib/data/gl-latlong-1km-landcover.bsq'
data = np.fromfile(fileName, dtype=np.uint8)
data = data.reshape(21600, 43200)
plt.imshow(data[::100,::100])
print(data)


half_horizantal = 43200/2
half_vertical = 21600/2

input_v = input('vertical coordinate (degree N/S): ')
if input_v[-1] == 'N':
    converted_v = int(half_vertical - (float(input_v[:-1])/(180/21600)))
elif input_v[-1] == 'S':
    converted_v = int(half_vertical + (float(input_v[:-1])/(180/21600)))

input_h = input('horizantal coordinate (degree E/W): ')
if input_h[-1] == 'E':
    converted_h = int(half_horizantal + (float(input_h[:-1])/(360/43200)))
elif input_h[-1] == 'W':
    converted_h = int(half_horizantal - (float(input_h[:-1])/(360/43200)))

print(f'{converted_v=}, {converted_h=}')


value = int(data[converted_v, converted_h])

value_label = {
    0:'Water',
    1:'Evergreen Needleleaf Forest',
    2:'Evergreen Braodleaf Forest',
    3:'Decisuous Needleleaf Forest',
    4:'Decisuous Braodleaf Forest',
    5:'Mixed Forest',
    6:'Woodland',
    7:'Wooded Grassland',
    8:'Closed Shrubland',
    9:'Open Shrubland',
    10:'Grassland',
    11:'Cropland',
    12:'Bare Ground',
    13:'Urban and Built',
}

print(value_label[value])


plt.show()

def testa():
    print("Code worked on Windows 10.0")
testa()